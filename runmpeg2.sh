#!/bin/bash

lli --use-mcjit ./benchmark/mpeg2dec_final.bc -b ./benchmark/input_base_4CIF_96bps.mpg -r -f -o0 ./tmp%d 2> /dev/null
mv ./output/out.tmp ./output/mpeg2dec.csv
rm ./tmp*
echo "output is in ./output/mpeg2dec.txt"
