#!/bin/bash
echo "compiling external profile function"
clang -O0 -emit-llvm -c tools/heap_profiler.c -o tools/heap_profiler.bc

#mpeg2
echo "building and instrumenting mpeg2dec benchmark"
llvm-link ./benchmark/mpeg2dec.bc tools/heap_profiler.bc -f -o ./benchmark/mpeg2dec_merged.bc
opt -load ../../../Debug+Asserts/lib/assignment3-bernales.so -assignment3-bernales < ./benchmark/mpeg2dec_merged.bc > ./benchmark/mpeg2dec_final.bc

#jpeg-6
echo "building and instrumenting jpeg-6a benchmark"
llvm-link ./benchmark/jpeg-6a.bc tools/heap_profiler.bc -f -o ./benchmark/jpeg-6a_merged.bc
opt -load ../../../Debug+Asserts/lib/assignment3-bernales.so -assignment3-bernales < ./benchmark/jpeg-6a_merged.bc > ./benchmark/jpeg-6a_final.bc

echo "Done."
