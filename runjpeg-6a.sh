#!/bin/bash

lli --use-mcjit ./benchmark/jpeg-6a_final.bc -dct int -progressive -opt ./benchmark/testimg.ppm > /dev/null
mv ./output/out.tmp ./output/jpeg-6a.csv
echo "output is in ./output/jpeg-6a.txt"
