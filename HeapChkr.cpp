#include "llvm/Pass.h"
#include "llvm/IR/Function.h"
#include "llvm/IR/Module.h"
#include "llvm/Support/raw_ostream.h"
#include "llvm/IR/Instructions.h"      
#include "llvm/IR/IRBuilder.h"
#include "llvm/ADT/StringRef.h"
#include <string.h>

using namespace llvm;

namespace llvm {
    struct HeapChkr : public ModulePass {
        static char ID;

    public:
        HeapChkr() : ModulePass(ID){ }

        virtual bool runOnModule(Module &M) 
        {    
            init(M);

            Module::FunctionListType &funcList = M.getFunctionList();
            for (Module::iterator funci = funcList.begin(); funci != funcList.end(); ++funci) 
            {
                Function &f = *funci;
                if(f.getBasicBlockList().size() > 0)
                {
                    for (Function::iterator bbI = f.begin(), bbE = f.end(); bbI != bbE; ++bbI) 
                    {
                        for (BasicBlock::iterator insI = *bbI->begin(), insE = *bbI->end(); insI != insE; ++insI )
                        {
                            if (CallInst* CI = dyn_cast<CallInst>(&(*insI)) )
                            {
                                Function *F = CI->getCalledFunction();
                                if(F) 
                                {
                                    StringRef name = F->getName();
                                    if(name.equals(_mallocStr) )
                                    {
                                        ++insI;
                                        addProfileMallocCall(*CI, *insI);
                                        --insI;
                                    }
                                    else 
                                        if(name.equals(_freeStr) )
                                        {
                                            addProfileFreeCall(*CI);
                                        }
                                } 
                            }
                        }
                    }
                }
            }

            return false;
        }

    private:
        StringRef _profileMallocStr;
        StringRef _profileFreeStr;
        Function* _profileMallocFunc;
        Function* _profileFreeFunc;
        StringRef _mallocStr;
        StringRef _freeStr;
        LLVMContext* _Context;

        void addProfileFreeCall(CallInst &freeIns)
        {
            std::vector<Value*> args;
            args.push_back(freeIns.getArgOperand(0));

            CallInst::Create(_profileFreeFunc, args, "", &freeIns);  
        }
        
        void addProfileMallocCall(CallInst &mallocInst, Instruction &nextIns)
        {
            // create the arguments for the profile call
            std::vector<Value*> args;
            args.push_back(mallocInst.getArgOperand(0));
            args.push_back(&mallocInst);

            CallInst::Create(_profileMallocFunc, args, "", &nextIns);  
        }

        void init(Module &M) 
        {
            _mallocStr = "malloc";
            _freeStr = "free";
            _profileMallocStr = "profileMalloc";
            _profileFreeStr = "profileFree";
            _Context = &(M.getContext());

            Module::FunctionListType &funcList = M.getFunctionList();
            for (Module::iterator funci = funcList.begin(); funci != funcList.end(); ++funci) 
            {
                Function &f = *funci;
                StringRef name = f.getName();
                if(name.equals(_profileMallocStr) )
                {
                    _profileMallocFunc = &f;
                }
                if(name.equals(_profileFreeStr) )
                {
                    _profileFreeFunc = &f;
                }
            }
        }

    };
}

char HeapChkr::ID = 0;
static RegisterPass<HeapChkr> X("assignment3-bernales", "Heap profiler", false, false);
