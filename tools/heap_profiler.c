#include <stdio.h>
#include <stdlib.h>

char outputFilename[] = "./output/out.tmp";
FILE *fp; 

void profileMalloc(size_t size, void* ptr) {
  if(!fp) {
    fp = fopen(outputFilename, "w"); 
  }
  fprintf(fp, "m, %p, %d\n", ptr, (int)size);
}

void profileFree(void* ptr) {
  fprintf(fp, "f, %p\n", ptr);
}
